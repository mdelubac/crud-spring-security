package com.controlleurs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.entity.Tache;
import com.service.TacheService;

@Controller
@RequestMapping(value="/tacheController")
public class TacheController {
	@Autowired
	TacheService tacheService;
	
	@RequestMapping(value="/index", method=RequestMethod.GET)
	public ModelAndView  findAllTache()
	{
		ModelAndView mv = new ModelAndView();
		mv.setViewName("Tache.html");
		mv.addObject("taches", tacheService.findAllTache());
		return mv;
	}
	
	@RequestMapping(value="/deleteTache", method=RequestMethod.GET)
	public ModelAndView deleteTache(@RequestParam(name="id") Long id)
	{
		tacheService.deleteTache(id);
		ModelAndView mv = new ModelAndView();
		mv.setViewName("Tache.html");
		mv.addObject("taches", tacheService.findAllTache());
		return mv;
		
	}
	@RequestMapping(value="/addTache", method=RequestMethod.POST)
	public ModelAndView addTache(@ModelAttribute Tache tache)
	{
		tacheService.addTache(tache);
		ModelAndView mv = new ModelAndView();
		mv.setViewName("Tache.html");
		mv.addObject("taches", tacheService.findAllTache());
		return mv;
	}
	@RequestMapping(value="/getOne", method=RequestMethod.GET)
	public ModelAndView findTacheById(@RequestParam(name="id") Long id)
	{
		ModelAndView mv = new ModelAndView();
		mv.setViewName("UpdateTache.html");
		mv.addObject("tache", tacheService.findByIdTache(id).orElse(null));
		
		return mv;
	}
	

}
