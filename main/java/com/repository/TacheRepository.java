package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Tache;

public interface TacheRepository extends JpaRepository<Tache, Long>{
 
}
