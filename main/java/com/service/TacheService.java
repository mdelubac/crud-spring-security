package com.service;

import java.util.List;
import java.util.Optional;

import com.entity.Tache;


public interface TacheService {
	public void addTache(Tache tache);
	public void updateTache (Tache tache);
	public void deleteTache(Long id);
	public Optional <Tache> findByIdTache(Long id);
	public List<Tache> findAllTache();

}
