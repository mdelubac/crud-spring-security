package com.service.imp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.entity.User;
import com.service.UserService;
@Service
@Transactional
public class CustomUserDetailsServiceImp implements UserDetailsService{
	
	@Autowired
	UserService userService;
	

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		User user = userService.findByName(username);
		System.out.println("nombre de roles :" + user.getRoles().size());
		if (user==null) throw new UsernameNotFoundException(username);
		
		List<GrantedAuthority> authorities = new ArrayList<>();
		user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getNomRole()));
		});
		
		return (UserDetails) new org.springframework.security.core.userdetails.User(user.getName(), user.getPassword(), authorities) ;
	}

}
