package com.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Tache;
import com.repository.TacheRepository;
import com.service.TacheService;

@Service
public class TacheServiceImp implements TacheService{
	@Autowired
	TacheRepository tacheRepository;

	@Override
	public void addTache(Tache tache) {
		// TODO Auto-generated method stub
		tacheRepository.save(tache);
		
	}

	@Override
	public void updateTache(Tache tache) {
		// TODO Auto-generated method stub
		tacheRepository.save(tache);
		
	}

	@Override
	public void deleteTache(Long id) {
		// TODO Auto-generated method stub
		tacheRepository.deleteById(id);;
	}



	@Override
	public List<Tache> findAllTache() {
		// TODO Auto-generated method stub
		return tacheRepository.findAll();
	}

	@Override
	public Optional<Tache> findByIdTache(Long id) {
		// TODO Auto-generated method stub
		return tacheRepository.findById(id);
	}



}
