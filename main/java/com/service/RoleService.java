package com.service;

import java.util.List;
import java.util.Optional;

import com.entity.Role;

public interface RoleService {
	public void addRole(Role role);
	public void updateRole (Role role);
	public void deleteRole(Long id);
	public Optional <Role> findByIdRole(Long id);
	public List<Role> findAllRole();
	

}
