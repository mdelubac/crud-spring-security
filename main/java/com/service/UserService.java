package com.service;

import java.util.List;
import java.util.Optional;

import com.entity.User;

public interface UserService {
	public void addUser(User user);
	public void updateUser (User user);
	public void deleteUser(Long id);
	public Optional <User> findByIdUser(Long id);
	public List<User> findAllUser();
	public User findByName(String name);

}
