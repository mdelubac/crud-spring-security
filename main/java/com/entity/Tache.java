package com.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="TACHE")
public class Tache implements Serializable{
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="ID_TACHE")
	private Long idTache;
	@Column(name="TITRE")
	private String titre;
	@Column(name="DESCRIPTION")
	private String description;
	@Column(name="DATE_AFFECTATION")
	private Date dateAffection;
	@Column(name="DATE_LIVRAISON")
	private Date dateLivraison;
	
	public Tache() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdTache() {
		return idTache;
	}

	public void setIdTache(Long idTache) {
		this.idTache = idTache;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateAffection() {
		return dateAffection;
	}

	public void setDateAffection(Date dateAffection) {
		this.dateAffection = dateAffection;
	}

	public Date getDateLivraison() {
		return dateLivraison;
	}

	public void setDateLivraison(Date dateLivraison) {
		this.dateLivraison = dateLivraison;
	}
	
	

	}
	
	


	
